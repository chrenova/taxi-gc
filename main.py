from flask import Flask, render_template

app = Flask(__name__)


from google.cloud import datastore

datastore_client = datastore.Client()

def fetch_tasks():
    query = datastore_client.query(kind='tasks')
    #query.order = ['-timestamp']
    tasks = query.fetch()
    return tasks


@app.route('/')
def root():
    tasks = fetch_tasks()
    return render_template('index.html', tasks=tasks)
